---
can_merge_usr: true
has_automatic_dbgsym: true
strip_source_version_suffix: bv20\d\d.*b\d+|b\w\d+
sources:
  - label: %DISTRO%-target
    apt_uri: '%MIRROR%'
    apt_components:
      - target
...
