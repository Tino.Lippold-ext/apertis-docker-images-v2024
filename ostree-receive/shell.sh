#!/bin/sh

# Make sure that OSTREE_RECEIVE_CONF is set if we're running ostree-receive.
if [ "$1" = '-c' ] \
    && [ "$(basename "$(echo "$2" | awk '{print $1}')")" = 'ostree-receive' ] \
    && [ -z "$OSTREE_RECEIVE_CONF" ]; then
  echo '[ERROR] OSTREE_RECEIVE_CONF is unset!' >&2
  echo "[ERROR] Set it via: '-o SetEnv=OSTREE_RECEIVE_CONF=...'" >&2
  exit 1
fi

exec sh "$@"
